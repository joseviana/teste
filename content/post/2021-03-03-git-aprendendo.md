---
title: Aprendendo Git e GitLab
date: 2021-03-03
---
## Instalando
```bash
apt-get install git
```

## Primeiros comandos

Comando | Descrição
:------- | :--------:
|git add | adiciona arquivos no versionamento  |
|git commit | envia alterações para o repositório local |
|git init   | cria um repositório  |
|git mv | renomeia ou move arquivos  |
|git push | enviar alterações para repositório remoto  |
|git rm | apaga arquivos |
|git checkout / git switch |  mudar a branch/ramo |

### Iniciando um repositório

```bash
  git init
```
